﻿
string text1 = "ABABDABACDABABCABAB";
string pattern1 = "ABAB";
List<int> expected1 = new List<int> { 0, 10 };
RunTest(text1, pattern1, expected1);

string text2 = "AABAACAADAABAABA";
string pattern2 = "AABA";
List<int> expected2 = new List<int> { 0, 9, 12 };
RunTest(text2, pattern2, expected2);

string text3 = "AAAAAAAAAAAA";
string pattern3 = "AAAAA";
List<int> expected3 = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7 };
RunTest(text3, pattern3, expected3);

void RunTest(string text, string pattern, List<int> expected)
{
    List<int> result = kmp(text, pattern);
    Console.WriteLine($"Text: {text}, Pattern: {pattern}");
    Console.WriteLine($"Expected: {string.Join(",", expected)}, Actual: {string.Join(",", result)}");
    Console.WriteLine($"Test Passed: {ListsAreEqual(expected, result)}\n");
}
bool ListsAreEqual(List<int> list1, List<int> list2)
{
    if (list1.Count != list2.Count)
        return false;
    for (int i = 0; i < list1.Count; i++)
    {
        if (!list1[i].Equals(list2[i]))
            return false;
    }
    return true;
}
List<int> kmp(string text,string pattern)
{
    var results = new List<int>();
    var prefix = new int[pattern.Length];
    int j = 0;
    for (int i = 1; i < pattern.Length; i++)
    {
        while (j > 0 && pattern[i] != pattern[j])
        {
            j = prefix[j - 1];
        }
        if (pattern[i] == pattern[j])
        {
            prefix[i] = ++j;
        }
    }
    j = 0;

    for (int i = 0; i < text.Length; i++)
    {
        while (j > 0 && text[i] != pattern[j])
        {
            j = prefix[j - 1];
        }
        if (text[i] == pattern[j])
        {
            j++;
            if (j == pattern.Length)
            {
                results.Add(i-pattern.Length+1);
                j = prefix[j - 1];
            }
        }
    }
    return results;
}